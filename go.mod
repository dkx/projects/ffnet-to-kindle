module dkx/ffnet-to-kindle

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/bmaupin/go-epub v0.5.1
	github.com/gammazero/workerpool v0.0.0-20190608213748-0ed5e40ec55e
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/gosimple/slug v1.6.0
	github.com/hashicorp/go-multierror v1.0.0
	github.com/jordan-wright/email v0.0.0-20190218024454-3ea4d25e7cf8
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
)
