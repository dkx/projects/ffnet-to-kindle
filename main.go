package main

import (
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/bmaupin/go-epub"
	"github.com/gammazero/workerpool"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/gosimple/slug"
	"github.com/hashicorp/go-multierror"
	"github.com/jordan-wright/email"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type book struct {
	id            int
	slug          string
	title         string
	author        string
	description   string
	chaptersCount int
}

type chapter struct {
	id      int
	title   string
	content string
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/ff/{book}/download", DownloadHandler)
	r.HandleFunc("/ff/{book}/send/{mail}", SendHandler)

	portStr := os.Getenv("PORT")
	if portStr == "" {
		portStr = "8080"
	}

	port, err := strconv.Atoi(portStr)
	if err != nil {
		panic(err)
	}

	srv := &http.Server{
		Handler: r,
		Addr:    fmt.Sprintf("0.0.0.0:%d", port),
	}

	log.Printf("Running server on port %d", port)

	log.Fatal(srv.ListenAndServe())
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("index.html")
	if err != nil {
		panic(err)
	}

	if err := tmpl.Execute(w, nil); err != nil {
		panic(err)
	}
}

func DownloadHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	includeHeadlines := r.URL.Query().Get("headlines") == "true"
	bookId, err := strconv.Atoi(vars["book"])
	if err != nil {
		panic(err)
	}

	fmt.Printf("Downloading book %d\n", bookId)

	book, data, err := createBook(bookId, includeHeadlines)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.mobi", book.slug))
	w.Header().Set("Content-Type", "application/x-mobipocket-ebook")

	http.ServeContent(w, r, fmt.Sprintf("%s.mobi", book.slug), time.Now(), bytes.NewReader(data))
}

func SendHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	includeHeadlines := r.URL.Query().Get("headlines") == "true"
	mail := vars["mail"]
	bookId, err := strconv.Atoi(vars["book"])
	if err != nil {
		panic(err)
	}

	fmt.Printf("Sending book %d\n", bookId)

	book, data, err := createBook(bookId, includeHeadlines)
	if err != nil {
		panic(err)
	}

	e := email.NewEmail()
	e.From = os.Getenv("SMTP_FROM")
	e.To = []string{mail}
	e.Subject = "convert"
	e.Text = []byte("")

	if _, err := e.Attach(bytes.NewBuffer(data), fmt.Sprintf("%s.mobi", book.slug), "application/x-mobipocket-ebook"); err != nil {
		panic(err)
	}

	auth := smtp.PlainAuth("", os.Getenv("SMTP_USER"), os.Getenv("SMTP_PASSWORD"), os.Getenv("SMTP_HOST"))
	if err := e.Send(fmt.Sprintf("%s:%s", os.Getenv("SMTP_HOST"), os.Getenv("SMTP_PORT")), auth); err != nil {
		panic(err)
	}

	tmpl, err := template.ParseFiles("sent.html")
	if err != nil {
		panic(err)
	}

	if err := tmpl.Execute(w, nil); err != nil {
		panic(err)
	}
}

func createBook(bookId int, includeHeadlines bool) (*book, []byte, error) {
	book, err := getBookData(bookId)
	if err != nil {
		return nil, nil, err
	}

	chapters, err := loadChapters(book, includeHeadlines)
	if err != nil {
		return nil, nil, err
	}

	e := epub.NewEpub(book.title)
	e.SetAuthor(book.author)
	e.SetDescription(book.description)
	e.SetIdentifier(fmt.Sprintf("ffnet-%d", book.id))

	for i := 0; i < len(chapters); i++ {
		if _, err := e.AddSection(chapters[i].content, chapters[i].title, "", ""); err != nil {
			return nil, nil, err
		}
	}

	id, err := uuid.NewRandom()
	if err != nil {
		return nil, nil, err
	}

	path := fmt.Sprintf("%s/%s.epub", os.TempDir(), id.String())
	fmt.Printf("Saving epub to %s\n", path)

	if err := e.Write(path); err != nil {
		return nil, nil, err
	}

	mobi, err := epubToMobi(id, path)
	if err != nil {
		return nil, nil, err
	}

	data, err := fileToMemory(mobi)
	if err != nil {
		return nil, nil, err
	}

	return book, data, nil
}

func getBookData(bookId int) (*book, error) {
	url := fmt.Sprintf("https://www.fanfiction.net/s/%d", bookId)
	fmt.Printf("Visiting %s\n", url)

	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, err
	}

	title := doc.Find("#profile_top > b").First().Text()
	author := doc.Find("#profile_top > a").First().Text()
	description := doc.Find("#profile_top > div").First().Text()

	chaptersOption := doc.Find("span #chap_select option").Last()
	chaptersValue, _ := chaptersOption.Attr("value")
	if chaptersValue == "" {
		chaptersValue = "1"
	}

	chaptersCount, err := strconv.Atoi(chaptersValue)
	if err != nil {
		return nil, err
	}

	return &book{
		id:            bookId,
		slug:          slug.Make(title),
		title:         title,
		author:        author,
		description:   description,
		chaptersCount: chaptersCount,
	}, nil
}

func loadChapters(book *book, includeHeadlines bool) ([]*chapter, error) {
	chapters := make([]*chapter, book.chaptersCount)

	var errs *multierror.Error
	wp := workerpool.New(5)

	for i := 1; i <= book.chaptersCount; i++ {
		chapterId := i
		wp.Submit(func() {
			if chapter, err := loadChapter(book, chapterId, includeHeadlines); err == nil {
				chapters[chapterId-1] = chapter
			} else {
				errs = multierror.Append(errs, err)
			}
		})
	}

	wp.StopWait()

	if err := errs.ErrorOrNil(); err != nil {
		return nil, err
	}

	return chapters, nil
}

func loadChapter(book *book, id int, includeHeadline bool) (*chapter, error) {
	url := fmt.Sprintf("https://www.fanfiction.net/s/%d/%d", book.id, id)
	fmt.Printf("Visiting %s\n", url)

	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, err
	}

	title := ""
	content, err := doc.Find("#storytext").First().Html()
	if err != nil {
		return nil, err
	}

	titleFull := doc.Find("span #chap_select option[selected]").First().Text()

	if titleFull != "" {
		re, err := regexp.Compile(`^\d+\.\s(.+)$`)
		if err != nil {
			return nil, err
		}

		title := re.FindStringSubmatch(titleFull)[1]

		if includeHeadline && book.chaptersCount > 1 {
			var contentBuilder strings.Builder
			contentBuilder.WriteString(fmt.Sprintf("<h4>%s</h4>", title))
			contentBuilder.WriteString(content)
			content = contentBuilder.String()
		}
	}

	return &chapter{
		id:      id,
		title:   title,
		content: content,
	}, nil
}

func epubToMobi(id uuid.UUID, path string) (string, error) {
	mobi := fmt.Sprintf("%s/%s.mobi", os.TempDir(), id.String())
	cmd := exec.Command("ebook-convert", path, mobi)

	fmt.Printf("Generating mobi with ebook-covert to %s\n", mobi)

	out, err := cmd.CombinedOutput()
	if err != nil {
		if _, existsErr := os.Stat(mobi); os.IsNotExist(existsErr) {
			fmt.Printf("Mobi file %s was not generated, error:\n", mobi)
			fmt.Println(string(out))
			return "", err
		}
	}

	fmt.Printf("Removing epub file %s\n", path)
	if err := os.Remove(path); err != nil {
		fmt.Println(err)
	}

	return mobi, nil
}

func fileToMemory(path string) ([]byte, error) {
	fmt.Printf("Loading mobi file to memory %s\n", path)
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	fmt.Printf("Removing movi file %s", path)
	if err := os.Remove(path); err != nil {
		fmt.Println(err)
	}

	return data, nil
}
