FROM golang:1.12.7-alpine AS build
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ENV GCO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

COPY . /app/
WORKDIR /app

RUN apk add --no-cache git build-base && \
	go mod download && \
	go build -a -tags 'osusergo netgo static_build' -ldflags '-extldflags "-static"' -o out/ffnet-to-kindle && \
	chmod +x out/ffnet-to-kindle

FROM debian:10.0
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

COPY --from=build /app/index.html /index.html
COPY --from=build /app/sent.html /sent.html
COPY --from=build /app/out/ffnet-to-kindle /ffnet-to-kindle

RUN apt update && \
	apt install -y calibre

CMD ["/ffnet-to-kindle"]
